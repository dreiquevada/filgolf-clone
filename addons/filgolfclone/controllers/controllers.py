# -*- coding: utf-8 -*-
from openerp import http

class Filgolfclone(http.Controller):
    @http.route('/filgolfclone', auth='public', website=True)
    def _home(self, **kw):
        Tournaments = http.request.env['filgolf.tournament']
        return http.request.render('filgolfclone.main_page', {
            'tournaments': Tournaments.search([])
        })

    @http.route('/filgolfclone/tournament-page', auth='public', website=True)
    def _tournament_page(self, **kw):
        Tournaments = http.request.env['filgolf.tournament']
        return http.request.render('filgolfclone.tournament_list', {
            'tournaments': Tournaments.search([])
        })

    @http.route('/filgolfclone/players', auth='public', website=True)
    def _players_page(self, **kw):
        Players = http.request.env['filgolf.player']
        return http.request.render('filgolfclone.player_list', {
            'players': Players.search([])
        })

    @http.route('/filgolfclone/<model("filgolf.tournament"):tournament>', auth='public', website=True)
    def _tournament_details(self, tournament, **kw):
        return http.request.render('filgolfclone.tournament_details', {
            'tournament': tournament
        })

    @http.route('/filgolfclone/courses', auth='public', website=True)
    def _courses_page(self, **kw):
        Courses = http.request.env['filgolf.course']
        return http.request.render('filgolfclone.course_list', {
            'courses': Courses.search([])
        })

    @http.route('/filgolfclone/players<model("filgolf.team"):team>', auth='public', website=True)
    def _team_members(self, team, **kw):
        return http.request.render('filgolfclone.team_members_list', {
            'team': team
        })
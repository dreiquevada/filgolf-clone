from datetime import date
from openerp import models, fields, api

class FilGolfTournament(models.Model):
    _name='filgolf.tournament'
    _rec_name='tournament_name'

    tournament_name = fields.Char(string="Name", required=True)
    tournament_course = fields.Many2one('filgolf.course', string="Course", required=True)
    tournament_course_name = fields.Char(related='tournament_course.course_name')
    tournament_rounds = fields.One2many('filgolf.rounds', 'tournament_id', string="Rounds")
    tournament_format = fields.Many2one('filgolf.format', string="Format", required=True)
    tournament_format_name = fields.Char(related='tournament_format.format_name')
    tournament_rules = fields.Text(string="Rules", required=True)
    tournament_startdate = fields.Date(string="Start Date", default=fields.Date.today)
    tournament_enddate = fields.Date(string="End Date", default=fields.Date.today)
    tournament_starttime = fields.Float(string="Start Time")
    tournament_endtime = fields.Float(string="End Time")
    tournament_division = fields.One2many('filgolf.division', 'tournament_id', string="Divisions")
    tournament_activeteam = fields.One2many('filgolf.activeteam', 'tournament_id', string="Register Teams")

    @api.multi
    def get_division(self, handicap):
        for record in self:
            classification_name = "Uncategorized Division"
            for classification in record.tournament_division:
                if ((handicap >= classification.division_max_handicap) and (handicap <= classification.division_min_handicap)):
                    classification_name = classification.division_name
                    break
            return classification_name

    # @api.multi
    # def get_team_list(self):
    #     for record in self:
    #         list_of_teams = []
    #         for team in record.tournament_activeteam:
    #             list_of_teams.append(team.team_name)
    #         return list_of_teams

class FilGolfFormat(models.Model):
    _name = 'filgolf.format'
    _rec_name = 'format_name'

    format_name = fields.Char()

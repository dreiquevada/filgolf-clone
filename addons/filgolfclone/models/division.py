from openerp import models, fields, api

class FilGolfDivision(models.Model):
    _name="filgolf.division"
    _rec_name="division_name"

    division_name = fields.Char()
    division_max_handicap = fields.Float()
    division_min_handicap = fields.Float()

    tournament_id = fields.Many2one('filgolf.tournament', ondelete='cascade', string="Tournament")
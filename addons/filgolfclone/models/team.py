from openerp import models, fields, api

class FilGolfTeam(models.Model):
    _name="filgolf.team"
    _rec_name="team_name"

    team_name = fields.Char(string="Team Name", required=True)
    team_members = fields.One2many('filgolf.player', 'team_id', string="Members")
    team_handicap = fields.Float(compute="_compute_handicap", string="Team Handicap")


    @api.multi
    def _compute_handicap(self):
        for record in self:
            if not record.team_members:
                record.team_handicap = 0
            else:
                total_handicap = 0
                for member in record.team_members:
	    			total_handicap += member.player_handicap
                record.team_handicap = total_handicap / len(record.team_members)


class FilGolfActiveTeam(models.Model):
    _name = 'filgolf.activeteam'
    _rec_name = 'activeteam_name'

    activeteam_name = fields.Many2one('filgolf.team', string="Team Name")
    activeteam_handicap = fields.Float(related="activeteam_name.team_handicap", string="Handicap")
    tournament_id = fields.Many2one('filgolf.tournament', ondelete='cascade', string="Tournament")
    team_division = fields.Char(compute="_get_team_division", string="Team Division")
    
    @api.depends('activeteam_handicap')
    def _get_team_division(self):
        for record in self:
            record.team_division = record.tournament_id.get_division(record.activeteam_handicap)
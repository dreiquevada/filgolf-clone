from openerp import models, fields, api

class FilGolfPlayer(models.Model):
    _name='filgolf.player'
    _rec_name="player_name"

    player_name = fields.Char(string="Name", required=True)
    player_handicap = fields.Float(default=0)

    team_id = fields.Many2one('filgolf.team', ondelete='cascade', string="Team")
from openerp import models, fields, api

class FilGolfRounds(models.Model):
    _name = 'filgolf.rounds'
    _rec_name = 'rounds_name'

    rounds_name = fields.Char(string="Name")
    rounds_holes = fields.One2many('filgolf.roundsholes', 'rounds_id', string="Holes")
    tournament_id = fields.Many2one('filgolf.tournament', ondelete='cascade', string="Tournament")

class FilGolfRoundsHoles(models.Model):
    _name = 'filgolf.roundsholes'
    _rec_name = 'roundsholes_name'

    roundsholes_name = fields.Char(string="Name")
    roundsholes_course = fields.Many2one('filgolf.course', string="Select Course")
    roundsholes_course_name = fields.Char(related="roundsholes_course.course_name")
    roundsholes_holes = fields.Many2one('filgolf.hole', string="Select Hole")
    rounds_id = fields.Many2one('filgolf.rounds', ondelete='cascade', string="Rounds")

    roundsholes_scores = fields.One2many('filgolf.score', 'roundsholes_id', string="Score")
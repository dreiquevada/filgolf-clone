from openerp import models, fields, api

class FilGolfCourse(models.Model):
    _name = 'filgolf.course'
    _rec_name = 'course_name'

    course_name = fields.Char(string="Venue")
    course_holes = fields.Integer(string="Number of Holes", compute='_course_holes')
    course_pars = fields.Integer(string="Total Pars", compute='_course_pars')
    course_hole = fields.One2many('filgolf.hole', 'course_id', string="Holes")
    course_hole_par = fields.Integer(related="course_hole.hole_par")

    @api.depends('course_hole')
    def _course_holes(self):
        self.course_holes = len(self.course_hole)

    @api.depends('course_hole', 'course_hole_par')
    def _course_pars(self):
        for hole in self.course_hole:
            self.course_pars += hole.hole_par
        return self.course_pars

class FilGolfHole(models.Model):
    _name = 'filgolf.hole'
    _rec_name = 'hole_name'

    hole_name = fields.Char(string="Name", required=True)
    hole_par = fields.Integer(string="Par", required=True)
    course_id = fields.Many2one('filgolf.course', ondelete='cascade', string="Course")
from openerp import models, fields, api

class FilGolfScore(models.Model):
    _name = 'filgolf.score'

    score_player = fields.Many2one('filgolf.player', string="Player")
    score_strokes = fields.Integer(string="Stokes")
    score_value = fields.Integer(string="Score", compute="_compute_score_value")

    roundsholes_id = fields.Many2one('filgolf.roundsholes', ondelete='cascade', string="Rounds")
    roundsholes_hole = fields.Many2one(related="roundsholes_id.roundsholes_holes")
    roundsholes_hole_value = fields.Integer(related="roundsholes_hole.hole_par")

    @api.depends('score_strokes','roundsholes_hole_value')
    def _compute_score_value(self):
        for record in self:
            record.score_value = record.score_strokes - record.roundsholes_hole_value
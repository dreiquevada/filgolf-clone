# -*- coding: utf-8 -*-
{
    'name': "FilGolf Clone",

    'summary': """
        A Better Version of Fil Golf website (filgolf.com)""",

    'description': """
        This module implements the basic features of Fil Golf website.
        On top of it, it also contains additional features such as 
        automatic tounament/match scheduling, assignment of divisions
        of different players.
    """,

    'author': "Ingen Trainees",
    'website': "http://www.a-better-version-of-filgolf.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Better Modules',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/division.xml',
        'data/format.xml',
        'views/tournament_view.xml',
        'views/division_view.xml',
        'views/course_view.xml',
        'views/team_view.xml',
        'views/player_view.xml',
        'views/tournament_teams_view.xml',
        'views/templates.xml',
        'views/rounds_view.xml',
        'views/score_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
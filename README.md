#**Requirements:**

- Docker CE
- Docker Compose

Installing Docker:

* [on Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/) , then follow [this](https://docs.docker.com/install/linux/linux-postinstall/) 



* [on Mac OS](https://docs.docker.com/docker-for-mac/install/)

* [on Windows](https://docs.docker.com/docker-for-windows/install/)



#**Getting Started**

* Clone this repository
* cd odoo9
* docker-compose up --build
* goto http://localhost:8069
* create a new database



##**Noob FAQs**

**What is Docker?**

Go [RTFM](https://docs.docker.com/engine/docker-overview/) :p

**How do I see all runnning containers?**

```bash
    docker ps
    
    # to see ONLY the running containers for the services defined in the .yml file:
    docker-compose ps
```
**How do I start all odoo9 containers?**

```bash
    cd odoo9
    docker-compose start
```

**How do I stop all odoo9 containers?**

```bash
    cd odoo9
    docker-compose stop
```

**How to open a shell session inside a container (to access its file system)?**

```bash
	# syntax: docker exec -ti <container name> /bin/bash

            docker exec -ti odoo9_odoo_1 /bin/bash
```

**How to open a shell session as root user inside a container (to access its file system)?**

```bash
    # syntax: docker exec -tiu root <container name> /bin/bash
            docker exec -tiu root odoo9_odoo_1 /bin/bash
```

**How to create a new Odoo module?**

```bash
     # 1) open a shell session in your odoo9 container
     # 2) cd to /mnt/extra-addons
     # 3) execute the scaffold command:
          $ odoo.py scaffold new_module_name
          # see the official docs (https://www.odoo.com/documentation/9.0/howtos/backend.html)
```

**Is it possible to use Python's debugger (pdb) in the Odoo container?**

Yes, just do the following:

    1) add the following lines under the service definition that runs Python (in the docker-compose.yml file)
    
```bash
    stdin_open: true
    tty: true
```

    2) re-run the containers

```bash
    docker-compose up -d
    
```
    3) attach a shell session to the running container
    
```bash
    # example:
    docker attach odoo9_odoo_1
    
    
    # OR...  do steps 2 and 3 in 1 single step with    docker-compose up -d &&  docker attach odoo9_odoo_1      
```